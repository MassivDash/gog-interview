import React from 'react'
import scss from './SliderControl.scss'


const SliderControl = ( props ) => (
    
        <li
          className={[scss.toggle, scss[props.liType]].join(' ')}
          onClick={props.clicked}
      
          >{props.children}</li>
      )
      
export default SliderControl 