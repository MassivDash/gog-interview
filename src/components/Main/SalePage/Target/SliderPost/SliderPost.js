import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Tracker from './Tracker/Tracker';
import scss from './SliderPost.scss';


class SliderPost extends Component {


    render () {
        let post = null;
        const progress = this.props.counter;

    


        const textStyle = {
            'fill': '#ffffff',
            'textAnchor': 'middle'
                };

        switch ( this.props.type ) {
            case ( 'validated' ):
                post = (<div className={scss.Post_container}>
                        <div className={scss.caption}>
                           
                        <p>{this.props.captionValidatedTitle}</p>
                        <p>{this.props.captionValidated}</p>
                        <div className={scss.thumbnail} >
                        <img src={this.props.image} />
                        </div>
                        </div>
                </div>);
                break;
            case ( 'notvalidated' ):
                post = (
                    post = <div className={scss.Post_container}>
                    <div className={scss.caption}>
                        <p>{this.props.captionNotValidatedTitle}</p>
                        <p>{this.props.captionNotValidated}</p>
                        <div className={scss.thumbnail} >
                        <img src={this.props.image} />
                        </div>
                        
                        </div>
                            <Tracker 
                            progress={progress}
                            startDegree={60}
                            progressWidth={8}
                            trackWidth={20}
                            cornersWidth={4}
                            size={80}
                            fillColor="transparent"
                            trackColor="transparent"
                            progressColor="white">

      <text x="200" y="200" style={textStyle}>{`${progress}%`}</text>

    </Tracker>


                        </div>
                
                );
                
            default:
            post = (
                post = <div className={scss.Post_container}>
                <div className={scss.caption}>
                    <p>{this.props.captionNotValidatedTitle}</p>
                    <p>{this.props.captionNotValidated}</p>
                    <div className={scss.thumbnail} >
                    <img src={this.props.image} />
                    </div>
                    
                    </div>
                        <Tracker 
                        progress={progress}
                        startDegree={60}
                        progressWidth={8}
                        trackWidth={20}
                        cornersWidth={4}
                        size={80}
                        fillColor="transparent"
                        trackColor="transparent"
                        progressColor="white">

  <text x="200" y="200" style={textStyle}>{`${progress}%`}</text>
  </Tracker></div>);
                
        }

        return post;
    }
}



export default SliderPost;