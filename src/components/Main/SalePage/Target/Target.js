import React, { Component } from 'react';
import scss from './Target.scss';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import CountUp from 'react-countup';
import AliceCarousel from 'react-alice-carousel';
import SliderPost from './SliderPost/SliderPost';
import SliderControl from './SliderControl/SliderControl';
import thumbnail from '../../Assets/Images/thumbnail.png';


@connect(state => ({ 
    counter: state.counter,
    validatedStep1: state.validatedStep1,
    validatedStep2: state.validatedStep2,
    validatedStep3: state.validatedStep3,
    validatedStep4: state.validatedStep4,
    validatedStep5: state.validatedStep5,


}))
class Target extends  Component  {
    
 
     
    
    static propTypes = {
        counter: PropTypes.shape({
          count: PropTypes.number.isRequired,
          validatedStep1: PropTypes.string.isRequired,
        validatedStep2: PropTypes.string.isRequired,
        validatedStep3: PropTypes.string.isRequired,
        validatedStep4: PropTypes.string.isRequired,
        validatedStep5: PropTypes.string.isRequired
          })};
    
      static defaultProps = {
        counter: {
          count: 252467,
        validatedStep1: "notvalidated",
        validatedStep2: "notvalidated",
        validatedStep3: "notvalidated",
        validatedStep4: "notvalidated",
        validatedStep5: "notvalidated"
      }}
    
      // Trigger the `INCREMENT_COUNTER` action in Redux, to add 1 to the total.
      // Note: by using the `= () {}` format, we're implicitly binding the component
      // to `this`, which is why we can use @connect's `.dispatch()` function that's
      // passed in as a prop
      triggerIncrement = () => {
        this.props.dispatch({
          type: 'INCREMENT_COUNTER',
        })
        this.Carousel._this._setInitialState()
      }

    componentWillUpdate() {
        console.log(this.props.counter.validatedStep1, this.props.counter.validatedStep2, this.props.counter.validatedStep3, this.props.counter.validatedStep4, this.props.counter.validatedStep5, "will upadte");
    } 
      
    componentDidMount (){
        console.log(this.props.counter.validatedStep1, this.props.counter.validatedStep2, this.props.counter.validatedStep3, this.props.counter.validatedStep4, this.props.counter.validatedStep5, "did mount");
    } 
      

    componentWillReceiveProps(){
        console.log(this.props.counter.validatedStep1, "will props");
    }

    componentWillMount() {
       
        console.log(this.props.counter.validatedStep1, "will mount");
    }


    
    componentDidUpdate() {
        console.log(this.props.counter.validatedStep1, this.props.counter.validatedStep2, this.props.counter.validatedStep3, this.props.counter.validatedStep4, this.props.counter.validatedStep5, "did upadte");
    } 
  
    render () {
        
        const { count } = this.props.counter;
       
        let tracker1 = (count / 10000)*100;
        let tracker2 = (count / 25000)*100;
        let tracker3 = (count / 50000)*100;
        let tracker4 = (count / 80000)*100;
        let tracker5 = (count / 120000)*100;

        console.log(tracker1,tracker2,tracker3,tracker4,tracker5); 
        console.log(this.props.counter.validatedStep1, this.props.counter.validatedStep2, this.props.counter.validatedStep3, this.props.counter.validatedStep4, this.props.counter.validatedStep5, "render");
      


        
          
let number = count,
    output = [],
    sNumber = number.toString();

for (let i = 0, len = sNumber.length; i < len; i += 1) {
    output.push(+sNumber.charAt(i));
}

let countUpMap = (
    output.map((output, i) => 
        <div key={i}  className={scss.digit_container} > 
            <CountUp 
                className={scss.sold} 
                duration={1} 
                start={0} 
                end={output} 
    /> 
            </div> )
);


let countUp = (  {countUpMap} );
      

        switch ( true ) {
    case  (count <= 9) :
    countUp = ( <div> 
        <div className={scss.digit_container} ><span className={scss.sold}>0</span></div>
        <div className={scss.digit_container} ><span className={scss.sold}>0</span></div>
        <div className={scss.digit_container} ><span className={scss.sold}>0</span></div>
        <div className={scss.digit_container} ><span className={scss.sold}>0</span></div>
        <div className={scss.digit_container}><span className={scss.sold}>0</span></div> 
        {countUpMap} 
        </div> 
       );
        break;
        case  (count <= 99) :
    countUp = ( <div>
        <div className={scss.digit_container} ><span className={scss.sold}>0</span></div>
        <div className={scss.digit_container} ><span className={scss.sold}>0</span></div>
        <div className={scss.digit_container} ><span className={scss.sold}>0</span></div>
        <div className={scss.digit_container} ><span className={scss.sold}>0</span></div>
        
        {countUpMap} 
         </div> 
       );
        break;
        case  (count <= 999) :
    countUp = ( <div> 
        <div className={scss.digit_container} ><span className={scss.sold}>0</span></div>
        <div className={scss.digit_container} ><span className={scss.sold}>0</span></div>
        <div className={scss.digit_container} ><span className={scss.sold}>0</span></div>
        {countUpMap} 
        
         </div> 
       );
        break;
        case  (count <= 9999) :
    countUp = ( <div>
        <div className={scss.digit_container} ><span className={scss.sold}>0</span></div>
        <div className={scss.digit_container} ><span className={scss.sold}>0</span></div>
        
                {countUpMap} 
        </div> 
       );
        break;
        case  (count <= 99999) :
    countUp = ( <div>
        <div className={scss.digit_container} ><span className={scss.sold}>0</span></div>
        
            {countUpMap} 
        </div> 
       );
        break;
        
        case  (count <= 999999) :
    countUp = ( <div>  {countUpMap} </div> 
       );
        break;
}

const responsive = {
    0: {
      items: 1
    },
    600: {
      items: 1
    },
    1024: {
      items: 1
    }
  };   
    
 


 
  

 
        return (
            
            <div className={scss.target_container} > 
                <div className={scss.title} >
                    Games sold so far
                    </div>

                <div className={scss.sold_container} >
                {countUp}

                
                </div>
                
        
                <AliceCarousel
            update={true}
          dotsDisabled={true}
          buttonsDisabled={true}
          responsive={responsive}
          infinite={false}
          
          ref={ el => this.Carousel = el }
        >
          <SliderPost type={this.props.counter.validatedStep1}
          captionValidatedTitle="Reach 10.000..."
          captionValidated="...to unlock exclusive, never before seen, trailer from Divinity: Original Sin." 
          captionNotValidatedTitle="Reach 10.000..."
          captionNotValidated="not validated"
          counter={tracker1}
          image={thumbnail}/>

          <SliderPost type={this.props.counter.validatedStep2}
          captionValidatedTitle="Reach 25.000..."
          captionValidated="...to unlock exclusive, never before seen, trailer from Divinity: Original Sin." 
          captionNotValidatedTitle="Reach 25.000..."
          captionNotValidated="not validated"
          counter={tracker2}
          image={thumbnail}/>

          <SliderPost type={this.props.counter.validatedStep3}
          captionValidatedTitle="Reach 50.000..."
          captionValidated="...to unlock exclusive, never before seen, trailer from Divinity: Original Sin." 
          captionNotValidatedTitle="Reach 55.000..."
          captionNotValidated="not validated"
          counter={tracker3}
            image={thumbnail}/>

          <SliderPost type={this.props.counter.validatedStep4}
          captionValidatedTitle="Reach 80.000..."
          captionValidated="...to unlock exclusive, never before seen, trailer from Divinity: Original Sin." 
          captionNotValidatedTitle="Reach 80.000..."
          captionNotValidated="not validated"
          counter={tracker4}
          image={thumbnail}/>

          <SliderPost type={this.props.counter.validatedStep5}
          captionValidatedTitle="Reach 120.000..."
          captionValidated="...to unlock exclusive, never before seen, trailer from Divinity: Original Sin." 
          captionNotValidatedTitle="Reach 120.000..."
          captionNotValidated="not validated"
          counter={tracker5}
          image={thumbnail}/>

        </AliceCarousel>
        
        
        <div className={scss.button+" "+scss.prev} onClick={() => this.Carousel._slidePrev()}></div>
        <div className={scss.button+" "+scss.next} onClick={() => this.Carousel._slideNext()}></div>
        <ul className={scss.toggle}> 
        <SliderControl liType={this.props.counter.validatedStep1} clicked={(event) => {this.Carousel._onDotClick(0)}}>10.000</SliderControl>
        <SliderControl liType={this.props.counter.validatedStep2} clicked={(event) => {this.Carousel._onDotClick(1)}}>25.000</SliderControl>
        <SliderControl liType={this.props.counter.validatedStep3} clicked={(event) => {this.Carousel._onDotClick(2)}}>50.000</SliderControl>
        <SliderControl liType={this.props.counter.validatedStep4} clicked={(event) => {this.Carousel._onDotClick(3)}}>80.000</SliderControl>
        <SliderControl liType={this.props.counter.validatedStep5} clicked={(event) => {this.Carousel._onDotClick(5)}}>120.000</SliderControl>
            </ul>
            

           
                
                </div>
        )
    }
}

export default Target