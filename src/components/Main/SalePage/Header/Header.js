import React from 'react';
import scss from './Header.scss';
import heart_support from '../../Assets/Images/heart_support.svg';
import mouse_play from '../../Assets/Images/mouse_play.svg';
import lock_drm from '../../Assets/Images/lock_drm.svg';
import clock_time from '../../Assets/Images/clock_time.svg';

const Header = () => (

<div className={scss.title_container}>
    <div className={scss.title}>
    Pay what you want for the <span>Divinity Bundle</span> ($32 value!)
    </div> 
    
    <div className={scss.items}>
                <div className={scss.item}><img src={heart_support} />Support Larian Studios</div>
                <div className={scss.item}><img src={mouse_play} />Play Divinity 2: DC before release</div>
                <div className={scss.item}><img src={lock_drm} />Get DRM-free games with goodies</div>
                <div className={scss.item}><img src={clock_time} />Only 23 : 54 : 55 left</div>
        </div>


    </div>

    

);
    
export default Header;