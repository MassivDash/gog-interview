import React, { Component } from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types'
import scss from './Hero.scss';
import GameFigure from './GameFigure/GameFigure';


import game1_divine_divinity from '../../Assets/Images/logo-inactive_DivineDivinity.png';
import game2_divine_divinity from '../../Assets/Images/logo-inactive_BeyondDivinity.png';
import game3_divine_divinity from '../../Assets/Images/logo-inactive_Divinity2.png';


import game1_active from '../../Assets/Images/logo-active_game1.png';
import game2_active from '../../Assets/Images/logo-active_game2.png';
import game3_active from '../../Assets/Images/logo-active_game3.png';
@connect(state => ({ counter: state.counter }))
class Hero extends Component {
    
        
        
    state = {
        value: 0.99,
        min_value: 0.99,
        avg_value: 7.67,
        ten_value:18.31,
        fin_value:49.99,
        unl_value:10000000,
        game1_validated:false, 
        game2_validated:false,
        game3_validated:false,

    }

    setValue() {
        this.setState( { value: this.refs.range.value } );
      }
    
    setEntry(){
        this.setState({ value: this.refs.number.value});
    }

    triggerIncrement = () => {
        this.props.dispatch({
          type: 'INCREMENT_COUNTER',
        });
      }
render () { 


let double = 2 * this.state.value;



let slide = 1.5* this.state.value;
let game1 = (
     <GameFigure 
         gameUrl={game1_divine_divinity}
         caption="Divine Divinity (normal price $5.99)  with 6 goodies and 4 language versions"
         validate="Below average" 
         style="notvalidated" />
             
);


let game2 = (
    
        <GameFigure 
        gameUrl={game2_divine_divinity}
         caption="Beyond Divinity (normal price $5.99) with 6 goodies and 4 language versions"
         validate="Above average (from $7.67)"
         style="notvalidated" />
            
);


let game3 = (
    
    <GameFigure 
         gameUrl={game3_divine_divinity}
         caption="Divinity 2 (preorder, normal price $19.99)  with 9 goodies and 7 language versions"
         validate="Top supporter (from $18.31)"
         style="notvalidated" />
);

if(this.state.value >= this.state.min_value ) {
     game1 = (
        <GameFigure 
            gameUrl={game1_active}
            caption="Divinity 2 (preorder, normal price $19.99) with 9 goodies and 7 language versions"
            validate="Below Average "
            style="validated" />
                
   );

   

}

if(this.state.value >= this.state.avg_value ) {
    game2 = (
      <GameFigure 
         gameUrl={game2_active}
         caption="Beyond Divinity (normal price $5.99) with 6 goodies and 4 language versions"
         validate="Above average (from $7.67)"
         style="validated" />
    );
}



if(this.state.value >= this.state.ten_value ) {
    game3 = (
      <GameFigure 
         gameUrl={game3_active}
         caption="Divinity 2 (preorder, normal price $19.99) with 9 goodies and 7 language versions"
         validate="Top supporter (from $18.31)"
         style="validated" />
    );
}

if(this.state.value >= 50) {
    slide = 75.5
};
 

if(this.state.value <= 0.98) {
    slide = 1.5
};
 
    return (
<div className={scss.hero_container} >
         
         <div className={scss.games_container} >

            {game1}
            {game2}
            {game3}         
        
        
         
         
 
        
 </div>
 <div className={scss.slider} >
          <div className={scss.min_value}>$0.99</div>
          <div className={scss.avg_value}>$7.67 (Average)</div>
          <div className={scss.top_value}>$18.31 (Top 10%)</div>
          <div className={scss.max_value}>$49.99</div>
          <input className={scss.special} style={{

background: "linear-gradient(90deg, rgba(154,188,0,0.6)" +double+"%,rgba(199,199,199,0.6)" + double+"%)",
background: "-webkit-gradient(linear, left top, right top, color-stop("+double+"%, rgba(154,188,0,0.6)), color-stop(" +double+"%, rgba(199,199,199,0.6)))",
background: "-webkit-linear-gradient(360deg, rgba(154,188,0,0.6)"+double+"%, rgba(199,199,199,0.6)"+double+"%)",
background: "-moz-linear-gradient(360deg, rgba(154,188,0,0.6)"+double+"%, rgba(199,199,199,0.6)" +double+ "%)",
background: "-ms-linear-gradient(360deg, rgba(154,188,0,0.6)"+double+"%, rgba(199,199,199,0.6)"+double+"%)",
background: "linear-gradient(90deg, rgba(154,188,0,0.6)"+double+"%, rgba(199,199,199,0.6)" +double+"%)"


          }}
          ref="range"
          type="range" 
          max={this.state.fin_value}
          min={this.state.min_value}
          value={this.state.value}
          step="0.1"
          onChange={ this.setValue.bind(this) }
          list="rangeList"
           />
          
      <div className={scss.button_wrapper} style={{
          margin: "-25px "+ slide+"%"
      }} > 
        
        <div className={scss.input} >
            <span className={scss.dollar}>
         $</span> <input
          ref="number" 
          type="number" 
          value={this.state.value}
          onChange={ this.setEntry.bind(this) } />
        
        </div>
        <button
        onClick={this.triggerIncrement}
        
        >Checkout now</button>  
                       
       </div>
       </div>
    
         </div>
    )
    
}



    

}

export default Hero;