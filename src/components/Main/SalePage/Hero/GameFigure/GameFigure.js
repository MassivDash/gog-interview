import React from 'react';
import scss from './GameFigure.scss';

const GameFigure = (props) => (


<figure>
            <div className={scss.image}>
            <img src={props.gameUrl} /></div>
                <div className={scss.caption}>{props.caption}</div>
            <div className={[scss.text, scss[props.style]].join(' ')}>
                {props.validate}
                </div>
</figure>

);

export default GameFigure; 