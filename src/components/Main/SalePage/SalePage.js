import React, { Component } from 'react';
import scss from './SalePage.scss';
import Header from './Header/Header';
import Hero from './Hero/Hero';
import Goodies from './Goodies/Goodies';
import Target from './Target/Target';

class SalePage extends Component {
render () { 
return (

<div className={scss.container}> 
    <Header />
    <Hero />
    <Goodies />
    <Target />
    
</div>

)
}

 }

 export default SalePage;
