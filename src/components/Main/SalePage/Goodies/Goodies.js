import React from 'react'
import Icon from './Icon/Icon';

import scss from './Goodies.scss';
import ico_journal from '../../Assets/Images/ico_journal.svg';
import ico_soundtrack from '../../Assets/Images/ico_soundtracks.svg';
import ico_stories from '../../Assets/Images/ico_stories.svg';
import ico_mac from '../../Assets/Images/ico_mac.svg';
import ico_movie from '../../Assets/Images/ico_movie.svg';
import ico_zipfile from '../../Assets/Images/ico_zipfile.svg';

const Goodies = () => (
    <div className={scss.goodies_container} > 
        
        <div className={scss.title} > 
        Goodies available for free with Divinity Bundle 
        </div>
       
        <div className={scss.icons} >
        <Icon iconUrl={ico_soundtrack} 
            alt="Over 3 hours of award winning music from all 3 games."
            title="4 soundtracks" 
            caption="Over 3 hours of award winning music from all 3 games." />
        <Icon 
            iconUrl={ico_stories}
            alt=" Prequel story for Divine Divinity and Beyond Divinity novella."
            title="2 short stories" 
            caption="Prequel story for Divine Divinity and Beyond Divinity novella." />
        <Icon 
            iconUrl={ico_journal}
            alt="Divinity 2 - Dev Journal 144 pages long book, detailing story and art of Divinity 2."
            title="Divinity 2 - Dev Journal" 
            caption=" 144 pages long book, detailing story and art of Divinity 2." />
        <Icon 
        iconUrl={ico_movie} 
        alt="Making of Divinity 2 40 minutes long, professional documentary about the development of Divinity 2."
        title="Making of Divinity 2" 
        caption=" 40 minutes long, profesional documentary about the development of Divinity 2." />
        <Icon 
            iconUrl={ico_mac}
            alt="7 wallpapers Beatifull, hand crafted HD wallpapers with Divine, Beyond and Divinity 2 art." 
            title="7 wallpapers"
            caption=" Beatifull, hand crafted HD wallpapers with Divine, Beyond and Divinity 2 art." />
        <Icon iconUrl={ico_zipfile} 
            alt="...and more 3 manuals, 56 artworks, 5 avatars, Beyond Divinity game-guide."
            title="...and more" 
            caption=" 3 manuals, 56 artworks, 5 avatars, Beyond Divinity game-guide." />
        </div>
    </div>
   
        
);

export default Goodies;
