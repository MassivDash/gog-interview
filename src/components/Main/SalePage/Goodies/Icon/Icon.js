import React from 'react';
import scss from './Icon.scss';


const Icon = (props) => (
<div className={scss.icon}> 
    <img src={props.iconUrl} alt={props.alt}/>
    <div className={scss.caption}>
        <p className={scss.title}>{props.title}</p>
        <p>{props.caption}</p>            
        </div>
    </div>

);

export default Icon; 