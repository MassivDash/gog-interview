// Sample reducer, showing how you can 'listen' to the `INCREMENT_COUNTER`
// action, and update the counter state

// Note: There's no need to specify default state, because the kit's Redux
// init code wraps `undefined` state values in a `defaultReducer()` function,
// that captures Redux sentinel vals and responds back with a black object --
// so in our reducer functions, we can safely assume we're working with 'real'
// immutable state

export default function reducer(state, action) {
  if (action.type === 'INCREMENT_COUNTER') {
    switch ( true ) {
      case ( state.count < 9999 ):
          return state.merge({
          count: state.count + 1 })
         
      case ( state.count == 9999 ): 
      return state.merge({
        count: state.count + 1, 
        validatedStep1: "validated"
      })
      break;
      case  (state.count < 24999) : 
      return state.merge({
        count: state.count + 1 })
        break;
      case ( state.count == 24999 ): 
      return state.merge({
        count: state.count + 1, 
        validatedStep2: "validated"
      })
      break;
      case  (state.count < 49999) : 
      return state.merge({
        count: state.count + 1 })
        break;
      case ( state.count == 49999 ): 
      return state.merge({
        count: state.count + 1, 
        validatedStep3: "validated"
      })
      break;
      case  (state.count < 79999) : 
      return state.merge({
        count: state.count + 1 })
        break;
      case ( state.count == 79999 ): 
      return state.merge({
        count: state.count + 1, 
        validatedStep4: "validated"
      })
      break;
      case  (state.count < 119999) : 
      return state.merge({
        count: state.count + 1 })
        break;
      case ( state.count == 119999 ): 
      return state.merge({
        count: state.count + 1, 
        validatedStep5: "validated"
      })
      break;
           
      default:
      return state.merge({
        count: state.count + 1 })
        
          
  }


}
  return state;
}
